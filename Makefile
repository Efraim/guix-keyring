DESTDIR ?=
PREFIX ?= /usr/local
SHAREDIR ?= $(DESTDIR)$(PREFIX)/share/guix-keyring
DATE ?= `date +%Y%m%d`

all: guix-keyring.gpg
	gpg --no-default-keyring --list-sigs --keyring ./guix-keyring.gpg | sig2dot > guix-keyring.dot
	dot -Tpdf guix-keyring.dot > guix-keyring.pdf
	dot -Tsvg guix-keyring.dot > guix-keyring.svg

update: guix-keyring.gpg
	gpg --no-default-keyring --keyring ./guix-keyring.gpg --refresh-keys

install:
	install -D guix-keyring.dot $(SHAREDIR)/guix-keyring.dot
	install -D guix-keyring.pdf $(SHAREDIR)/guix-keyring.pdf
	install -D guix-keyring.gpg $(SHAREDIR)/guix-keyring.gpg
	install -D guix-keyring.gpg $(SHAREDIR)/pubring.gpg

make-dist: guix-keyring.gpg
	git tag -s $(DATE) -m $(DATE)

environment:
	guix environment --ad-hoc gnupg graphviz signing-party wget git

clean:
	rm guix-keyring.gpg guix-keyring.gpg~ guix-keyring.dot guix-keyring.pdf pubring.kbx || true

.PHONY: all update install make-dist environment clean
