#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh

download-key ()
{
    wget "https://savannah.gnu.org/people/viewgpg.php?user_id=$1" -O keys/$2.key
}

download-key 98532 alexvong1995
download-key 96437 alezost
download-key 113946 ambrevar
download-key 89983 andreas
download-key 127547 apteryx
download-key 108492 arunisaac
download-key 98003 atheia
download-key 93889 bavier
download-key 102268 benwoodcroft
download-key 8976 biscuolo
download-key 107534 boskovits
download-key 105895 cbaines
download-key 15145 civodul
download-key 58917 cwebber
download-key 106231 dannym
download-key 92930 davexunit
download-key 99183 efraim
download-key 130692 hoebjo
download-key 106156 htgoebel
download-key 147297 ipetkov
download-key 97645 iyzsong
download-key 4348 janneke
download-key 86649 jlicht
download-key 103728 kkebreau
download-key 102024 lfam
download-key 115004 lfla
download-key 136574 lsl88
download-key 106836 marusich
download-key 106176 mbakke
download-key 128 mhw
download-key 108789 mothacehe
download-key 105077 nckx
download-key 97675 ngz
download-key 132491 pgarlick
download-key 96758 phant0mas
download-key 98021 rekado
download-key 115301 rhelling
download-key 90521 roelj
download-key 103528 roptat
download-key 141916 samplet
download-key 82041 sleep_walker
download-key 107858 snape
download-key 85997 taylanub
# download-key 123358 tsholokhova
download-key 144060 vagrantc
download-key 114039 wigust
download-key 20118 wingo

# and the inactive members
download-key 97298 beffa
download-key 5461 jmd
download-key 87873 nikita
download-key 109656 reepca
download-key 99939 remibd
download-key 88639 steap
download-key 107969 thomasd
download-key 97696 toothbrush
